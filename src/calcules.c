#include <stdlib.h> 
#include <stdio.h> 
#include <SDL/SDL.h> 
#include <math.h>
#include "calcules.h" 
#include <SDL/SDL_gfxPrimitives.h> 
#include <SDL/SDL_gfxPrimitives_font.h> 

void face(Coordonnees C, Coordonnees B, Coordonnees G, Coordonnees F, SDL_Surface* ecran, Uint32 couleur)
{
	if (((C.x - B.x)*(G.y - C.y) - (C.y - B.y)*(G.x - C.x)) >= 0) 
	{
		const Sint16 x[4] = { C.x, G.x, F.x, B.x };
		const Sint16 y[4] = { C.y, G.y, F.y, B.y };
		filledPolygonColor(ecran, x, y, 4, couleur);
	}
}

void angle(long xA, long yA, long zA, Coordonnees* A, double phi, double theta, double alpha)
{
	double XA, YA, ZA, xa, ya, za;

	XA = cos(alpha) * xA - sin(alpha) * yA;
	YA = sin(alpha) * xA + cos(alpha) * yA;
	ZA = zA;

	xa = XA;
	ya = cos(theta) * YA - sin(theta) * ZA;
	za = sin(theta) * YA + cos(theta) * ZA;

	A->x = cos(phi) * xa + sin(phi) * za;
	A->y = ya;
	A->z = cos(phi) * za - sin(phi) * xa;


	A->x = ceil(((8 / (A->z + 9)) * A->x) * 125 + 400);
	A->y = ceil(((8 / (A->z + 9)) * A->y) * 125 + 350);
}

#include <stdlib.h> 
#include <stdio.h> 
#include <SDL/SDL.h> 
#include <math.h> 
#include "calcules.h" 
#include <SDL/SDL_gfxPrimitives.h> 
#include <SDL/SDL_gfxPrimitives_font.h> 

void pause();
void face();

int main(int argc, char *argv[])
{
	int n = 0;
	int delay = 10;

	double phi = 0;
	double theta = 0;
	double alpha = 0;
	const Sint16 x[4] = { 200, 200, 600, 600 };
	const Sint16 y[4] = { 150, 550, 550, 150 };

	Coordonnees  D, A, B, C, H, E, F, G;

	SDL_Event event;
	SDL_Surface *ecran = NULL;
	SDL_Rect position;
	SDL_Init(SDL_INIT_VIDEO);
	ecran = SDL_SetVideoMode(800, 700, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);
	SDL_WM_SetCaption("The Cube  (v2.01) - Frederic Vitzikam", NULL);
	SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 0, 0, 0));

	SDL_PollEvent(&event);

	while (event.type != SDL_QUIT)
	{
		angle(-1, 1, 1, &D, phi, theta, alpha);
		angle(-1, -1, 1, &A, phi, theta, alpha);
		angle(1, -1, 1, &B, phi, theta, alpha);
		angle(1, 1, 1, &C, phi, theta, alpha);
		angle(-1, 1, -1, &H, phi, theta, alpha);
		angle(-1, -1, -1, &E, phi, theta, alpha);
		angle(1, -1, -1, &F, phi, theta, alpha);
		angle(1, 1, -1, &G, phi, theta, alpha);

		face(C, B, G, F, ecran, 0x0000FFFF);
		face(A, D, E, H, ecran, 0x00FF00FF);
		face(A, B, D, C, ecran, 0xFF0000FF);
		face(D, C, H, G, ecran, 0xFF8000FF);
		face(B, A, F, E, ecran, 0xDD00DDFF);
		face(E, H, F, G, ecran, 0x75DDFFFF);

		phi = sin((double)n*3.141592654 / 351)*3.141592654;
		theta = sin((double)n*3.141592654 / 300)*3.141592654;
		alpha = sin((double)n*3.141592654 / 289)*3.141592654;
		n++;

		SDL_Flip(ecran);
		SDL_Delay(delay);

		filledPolygonColor(ecran, x, y, 4, 0x000000FF);
		SDL_PollEvent(&event);
	}

	SDL_Flip(ecran);
	SDL_Quit();

	return EXIT_SUCCESS;
}

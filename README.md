# The Cube

<br>
<img src="https://gitlab.com/FrVi/Cube/raw/master/images/cube.png" alt="Drawing" width="403"/>
<br>

<br>
## Principle

The Rotation of a cube in 3D.

<br>
## The Program

The program is based on SDL_gfx, their is no OpenGL in it: it is drawing 2D triangles.<br>
<br>
I made this in 2007 while in high school. 

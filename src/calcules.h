#ifndef CUBE_H
#define CUBE_H

#include <stdlib.h> 
#include <stdio.h> 
#include <SDL/SDL.h> 
#include <math.h> 

void face();
void angle();

typedef struct Coordonnees Coordonnees;
struct Coordonnees
{
    double x; 
    double y; 
    double z; 
};

#endif//CUBE_H
